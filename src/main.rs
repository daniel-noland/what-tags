use git2::{Branch, BranchType, Repository};
use std::collections::HashMap;
use std::str;

fn get_docker_tags(
    head: String,
    head_branches: Vec<String>,
    head_tags: Vec<String>,
    all_tags: Vec<String>,
) -> Vec<String> {
    let mut docker_tags = head_branches.to_owned();

    // first 8 characters of the sha as a string
    // this is safe because there are no non-ASCII characters in SHAs
    // if that changes let me know
    docker_tags.push(String::from(&head[0..8]));

    for head_tag in head_tags {
        assert!(all_tags.contains(&head_tag));
        if let Ok(head_semver) = lenient_semver::parse(&head_tag) {
            // head_semver is a semver tag on HEAD, so we need to check for subcomponents to update
            let mut semversion_tags: Vec<_> = all_tags
                .iter()
                .flat_map(|tag| lenient_semver::parse(tag))
                .collect();
            semversion_tags.sort();

            docker_tags.push(head_semver.to_string());

            if let Some(highest_major_match) = semversion_tags
                .iter()
                .filter(|tag| tag.major == head_semver.major)
                .last()
            {
                if highest_major_match == &head_semver {
                    docker_tags.push(head_semver.major.to_string())
                }
            }

            if let Some(highest_minor_match) = semversion_tags
                .iter()
                .filter(|tag| tag.major == head_semver.major && tag.minor == head_semver.minor)
                .last()
            {
                if highest_minor_match == &head_semver {
                    docker_tags.push(format!("{}.{}", head_semver.major, head_semver.minor))
                }
            }

            if let Some(highest_version) = semversion_tags.iter().last() {
                if highest_version == &head_semver {
                    docker_tags.push(String::from("latest"))
                }
            }
        } else {
            docker_tags.push(head_tag.clone());
        }
    }

    return docker_tags
        .iter()
        .map(|tag| slugify(tag))
        .collect::<Vec<_>>();
}

fn slugify(input: &String) -> String {
    return if input.len() > 128 {
        input.to_ascii_lowercase()[..128].to_string()
    } else {
        input.to_ascii_lowercase()[..].to_string()
    }
    .replace(|c: char| !c.is_alphanumeric() && c != '.' && c != '-', "-")
    .trim_end_matches('-')
    .to_string();
}

fn main() {
    let loc = ".";
    let repo = Repository::open(loc).expect(&format!("Could not open repo at {}", loc));

    let head = repo
        .head()
        .expect("Could not find HEAD?")
        .peel_to_commit()
        .expect("HEAD was not a commit.  You're on your own here, boss.");

    let mut head_branches: Vec<String> = repo
        .branches(None)
        .expect("Could not get a branch list?")
        // unwrap Result<Branch, BranchType, Error>
        .flatten()
        // Filter to branches that point to HEAD
        .filter(|(branch, _): &(Branch, BranchType)| {
            branch
                .get()
                .peel_to_commit()
                .map_or(false, |c| c.id() == head.id())
        })
        .map(|(branch, btype)| {
            let branch_name = branch.name().unwrap().unwrap().to_string();
            match btype {
                // If the branch is local, we good
                BranchType::Local => branch_name,
                // If it's remote, trim off the remote
                BranchType::Remote => branch_name.split_once("/").unwrap().1.to_string(),
            }
        })
        // HEAD will always point to HEAD, obviously - we don't want it
        .filter(|name| name.to_ascii_lowercase() != "head")
        .collect();
    head_branches.sort();
    head_branches.dedup();
    let head_branches = head_branches; // no longer mutable

    let mut all_tags = HashMap::new();
    let _ = repo.tag_foreach(|id, name| {
        all_tags.insert(
            str::from_utf8(name)
                .unwrap()
                .to_owned()
                .replace("refs/tags/", ""),
            id.to_owned(),
        );
        return true;
    });

    let head_tags: Vec<String> = all_tags
        .iter()
        .filter(|i| i.1 == &head.id())
        .map(|i| i.0.to_owned())
        .collect();

    for tag in get_docker_tags(
        head.id().to_string(),
        head_branches,
        head_tags,
        all_tags.keys().map(|k| k.to_owned()).collect::<Vec<_>>(),
    ) {
        println!("{}", tag)
    }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;
    #[test]
    fn when_given_boring_values_for_all_inputs_they_are_all_in_the_output() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec!["boring-branch".to_string()],
            vec!["boring-tag".to_string()],
            vec!["boring-tag".to_string()],
        );
        assert_that!(&results).contains(&"da54030c".to_string());
        assert_that!(&results).contains(&"boring-tag".to_string());
        assert_that!(&results).contains(&"boring-branch".to_string());
    }

    #[test]
    fn when_the_branch_name_contains_special_and_uppercased_characters_it_is_slugified_in_the_output(
    ) {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec!["Branch$With!Shenanigans@".to_string()],
            vec![],
            vec![],
        );
        assert_that!(&results).contains(&"branch-with-shenanigans".to_string());
    }

    #[test]
    fn when_we_are_not_on_a_branch_or_tagged_commit_we_only_get_the_commit_sha() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![],
            vec![],
        );
        assert_that!(&results).contains(&"da54030c".to_string());
        assert_that!(&results).has_length(1);
    }

    #[test]
    fn when_the_branch_name_is_more_than_128_characters_it_is_truncated() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec!["a".repeat(129)],
            vec![],
            vec![],
        );
        assert_that!(&results).contains("a".repeat(128));
    }

    #[test]
    fn when_there_is_only_one_git_tag_and_it_is_a_semver_all_of_its_components_are_in_the_output() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.2.3")],
            vec![String::from("1.2.3")],
        );
        assert_that!(&results).contains_all_of(&vec![
            &String::from("1"),
            &String::from("1.2"),
            &String::from("1.2.3"),
        ]);
    }

    #[test]
    fn when_there_are_multiple_1_x_y_tags_and_head_is_the_latest_of_them_1_is_in_the_output() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.2.3")],
            vec![
                String::from("1.0.0"),
                String::from("1.1.1"),
                String::from("1.2.3"),
            ],
        );
        assert_that!(&results).contains_all_of(&vec![
            &String::from("1"),
            &String::from("1.2"),
            &String::from("1.2.3"),
        ]);
    }

    #[test]
    fn when_there_are_multiple_1_x_y_tags_and_head_is_not_the_latest_of_them_1_is_not_in_the_output(
    ) {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.2.3")],
            vec![
                String::from("1.0.0"),
                String::from("1.1.1"),
                String::from("1.2.3"),
                String::from("1.4.6"),
            ],
        );
        assert_that!(&results).contains_all_of(&vec![&String::from("1.2"), &String::from("1.2.3")]);
        assert_that!(&results).does_not_contain(&String::from("1"));
    }

    #[test]
    fn lenient_semver_can_parse_v_prefixed_semvers() {
        assert_that!(lenient_semver::parse("v1.2.3")).is_ok().is_equal_to(semver::Version::new(1,2,3))
    }

    #[test]
    fn v_prefixed_versions_are_okay() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("v1.2.3")],
            vec![String::from("v1.2.3")],
        );
        assert_that!(&results).contains_all_of(&vec![
            &String::from("1"),
            &String::from("1.2"),
            &String::from("1.2.3"),
        ]);
    }
}
